# Sample Perl script to transmit number
# to Arduino then listen for the Arduino
# to echo it back
use strict;
use List::Util qw(min max sum);
use Device::SerialPort;

# Set up the serial port
# 19200, 81N on the USB ftdi driver
my $port = Device::SerialPort->new("/dev/ttyUSB0");
$port->databits(8);
#$port->baudrate(38400);
$port->baudrate(115200);
$port->parity("none");
$port->stopbits(1);

# from https://github.com/adafruit/Tweet-a-Watt/blob/master/wattcher.py
#my $VREF = 354; #3.9 to 4.1
#my $VREF = 270; # 3.4
#my $VREF = 492; # 3.4
#my $VREF = 0; # 20
#my $VREF = 512; # 12
#my $VREF = 490; # 11
#my $VREF = 236; # 5.2
#my $VREF = 118; # 12
#my $VREF = 60; # 16
#my $VREF = 400; # 5.9
#my $VREF = 500; # 11
#my $VREF = 600; # 18
#my $VREF = 700; # 25
#my $VREF = 450; # 8.9
#my $VREF = 425; # 6.9
#my $VREF = 475; # 10
#my $VREF = 375; # 4.8
#my $VREF = 350; # 4
#my $VREF = 325; # 3.7
#my $VREF = 300; # 3.3
#my $VREF = 290; # 3.2
#my $VREF = 280; # 3.2
#my $VREF = 260; # 3.2
my $VREF = 285; # 3.2
#my $VREF = 260;

my $CURRENTNORM = 15.5 ;
my $MAINSVPP = 170*2; # From MAINSVPP = 170 * 2 # +-170V is what 120Vrms ends up being (= 120*2sqrt(2))
#my $assumedrate = 440;
#my $assumedrate = 20;
my $assumedrate = 1200;

my @finishing = ();

my @voltage = ();
my @amperage = ();
my @wattage = ();
sub finish {
	foreach my $sub (@finishing) {
		&$sub();
	}
}
$SIG{TERM} = \&finish;
sub avg {
	return sum(@_)/scalar(@_);
}
sub multiply {
	my ($a,$b) = @_;
	die "Not equal size!" unless scalar(@$a) == scalar(@$b);
	my $s = scalar(@$a);
	my @o = @$a;
	for (my $i = 0 ; $i < $s ; $i++ ) {
		$o[$i] = $o[$i] * $b->[$i];
	}
	return @o;
}
my $minv = 1024;
my $maxv = 0;

sub rms {
	return sqrt(avg(map { $_ * $_ } @_));
}

sub calculatePower {
	my ($n,$voltage,$amperage) = @_;
	my @voltage = @$voltage[0..($n-1)];
	my @amperage =@$amperage[0..($n-1)];
	my $minv = min($minv,@voltage);
	my $maxv = max($maxv,@voltage);
	my $avgv = avg($maxv,$minv);
	my $vpp = $maxv - $minv;
	@voltage = map { ( $_ - $avgv ) * $MAINSVPP / $vpp  } @voltage;
	@amperage = map { ($_ - $VREF) / $CURRENTNORM } @amperage;
	my @watts = multiply(\@voltage, \@amperage);
	my $avgamp = avg(map { abs($_) } @amperage) - 3.99;
	my $avgvolt = avg(map { abs($_) } @voltage);
	my $avgwatt = avg(map { abs($_) } @watts);
	my $vrms = rms(@voltage);
	my $arms = rms(@amperage) - 4.99;
	return ($avgwatt,$avgvolt,$avgamp,$avgamp * $avgvolt,$vrms,$arms,$vrms * $arms)
}

my $count = 0;
$| = 1;
open(my $fd, ">", "out.log");
open(my $fd2, ">", "raw.log");
push @finishing, sub { close($fd); };
push @finishing, sub { $port->close() if $port; };
while (1) {
	# Poll to see if any data is coming in
        my $char = $port->lookfor();
	#my $char = int(rand(1024))."\t".int(rand(1024))."\t".int(rand(1024));
	# If we get data, then print it
	# Send a number to the arduino
	if ($char) {
		my $t = time();
		my ($voltage,$amperage,$vref) = split(/\t/,$char);
		print $fd2 "$t\t$char$/";
		unshift @voltage, $voltage;
		unshift @amperage, $amperage;
		if (@voltage > $assumedrate) {
			my @o =
				calculatePower($assumedrate,\@voltage,
					\@amperage);
			my $str = 
			join("\t",$t,@o,$voltage,$amperage,$vref).$/;
			print $fd $str;
			print $str;
			@voltage = ();#@voltage[0..(@voltage/2)];
			@amperage = ();#@amperage[0..(@amperage/2)];
			#print "$VREF @o$/";
		}
		#print time()."\t$char$/";
	}
}
close($fd);
END {
	finish();
}
