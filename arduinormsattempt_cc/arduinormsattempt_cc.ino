/*
  AnalogReadSerial
 Reads an analog input on pin 0, prints the result to the serial monitor 
 
 This example code is in the public domain.
 */
#define N 64
int  amps[N];
int volts[N];
int ampsum = 0.0;
float voltsum = 0.0;
int iterator = 0;
float arms = 0.0;
float vrms = 0.0;
void setup() {
  Serial.begin(9600);
  for (int i = 0 ; i < N; i++) {
     amps[i] = volts[i] = 0; 
  }
  voltsum=ampsum=0;
}

void loop() {
  int ampValue  = analogRead(A0);
  int voltValue = analogRead(A1);
  voltsum = (float)(voltValue*voltValue) + voltsum - (float)(volts[iterator]*volts[iterator]);
  ampsum = (float)(ampValue*voltValue) + ampsum - (float)(amps[iterator]*amps[iterator]);  
  volts[iterator] = voltValue;
  amps[iterator] = ampValue;
  float vavg = voltsum / N;
  float aavg = ampsum / N;
  vrms = sqrt( vavg );
  arms = sqrt( aavg );
  iterator = iterator + 1;
  if (iterator >= N) { 
    iterator = 0; 
    Serial.print(vrms);
    Serial.print("\t");
    Serial.println(arms);
    //String vstr = String((int)vrms);
    //String astr = String((int)arms);
    //vstr += "\t";
    //vstr += astr;    
    //Serial.println(vstr);
  }
}


